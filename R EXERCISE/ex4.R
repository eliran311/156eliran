install.packages('ggplot2')
library(ggplot2)

install.packages("ggplot2movies")
library(ggplot2movies)

trmovies<-movies
str(EXmovies)

trmovies$Action <- factor(trmovies$Action,levels=c(0,1),  labels = c( "No Action", "Action"))
ggplot(trmovies, aes(Action, rating)) + geom_col()
#Q1
ggplot(trmovies, aes(rating))+geom_histogram()
#������� ������� �����
#Q2
ggplot(trmovies, aes(year,rating))+stat_smooth(method=lm)

#Q3
ggplot(trmovies, aes(year,budget))+stat_smooth(method=lm)

#Q4
breaks <- c(1,60,100,160,240,999)
labels <- c("very short length","short length", "regular length", "long length", "very long length")
bins <- cut(trmovies$length, breaks, include.lowest = T,right = F, labels=labels)
trmovies$length<-bins
ggplot(trmovies, aes(length,budget))+ geom_boxplot()

#Q5
ggplot(trmovies, aes(length,rating))+ geom_boxplot()

#Q6
breaks <- c(1,3,5,8,11,13)
labels <- c("very low","low", "medium", "medium plus", "good","very good")
bin <- cut(trmovies$length, breaks, include.lowest = T,right = F, labels=labels)
trmovies$rating<-bin
ggplot(EXmovies, aes(rating, budget))+geom_boxplot()

#Q7
EXmovies$Animation <- factor(trmovies$Animation,levels=c(0,1),  labels = c( "No Animation", "Animation"))
ggplot(trmovies,aes(Animation,rating))+geom_col()

#Q8
trmovies$Action <- factor(trmovies$Action,levels=c(0,1),  labels = c( "No Action", "Action"))
ggplot(EXmovies, aes(Action, rating)) + geom_col()
