<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/hello', function () {
    return "Hello World!!";
});
Route::get('/student/{id?}', function ($id = 'no student provided') {
    return "Hello Student ".$id;
});

Route::get('/customer/{id?}', function ($id= null) {
    if ($id==null)
    return view('nocustomer',['id' => $id]);
    else
    return view('customer',['id' => $id]);
});
route::resource('books','Todocontroller')->middleware('auth');
//route::get('books/done/{id}','Todocontroller@done')->name('done');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
