@extends('layouts.app')
@section('content')

<h1>edit a book</h1>
<form method = 'post' action = "{{action('Todocontroller@update', $book->id)}}"  >
@csrf
@method('PATCH')
<div class = "form-group">
    <label for = "title"> book to update </label>
    <input type = "text" class = "form-control" name = "title" value = "{{$book->title}}">
    <input type = "text" class = "form-control" name = "author" value = "{{$book->author}}">
</div>

<div class = "form-group">
 <input type = "submit" class= "form-control" name="submit" value= "Update">
</div>

</form>

<form method = 'post' action = "{{action('Todocontroller@destroy',$book->id)}}" >

@csrf   
@method('DELETE')    
<div class = "form-group">    
    <input type = "submit" class = "form-control" name = "submit" value = "Delete">
</div>

</form>

@endsection