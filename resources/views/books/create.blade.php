

@extends('layouts.app')
@section('content')

<h1>Create a new book</h1>
<form method = 'post' action = "{{action('Todocontroller@store')}}" >
{{csrf_field()}}      
    

<div class = "form-group">
    <label for = "title"> book name? </label>
    <input type = "text" class = "form-control" name = "title"><ul>
@foreach($errors->all() as $error)
   <li>{{$error}}</li>
@endforeach
</ul>

    <label for = "author"> book author? </label>
    <input type = "text" class = "form-control" name = "author">
</div>

<div class = "form-group">    
    <input type = "submit" class = "form-control" name = "submit" value = "Save">
</div>

</form>

@endsection



